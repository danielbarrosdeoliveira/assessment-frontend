import React from 'react';
import styles from './Busca.module.css';

const Busca = () => {
  return (
    <div className={styles.busca}>
      <input type="text" placeholder="Faça sua busca aqui..." />
      <button className={styles.button}>Buscar</button>
      <div className="buscaMobile">
        <input
          id="inputMobile"
          type="text"
          placeholder="Faça sua busca aqui..."
        />
        <button className={styles.iconeBusca}>
          <p>Busca</p>
        </button>
      </div>
    </div>
  );
};

export default Busca;
