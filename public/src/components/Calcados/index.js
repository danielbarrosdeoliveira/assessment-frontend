import React from 'react';
import styles from './Calcados.module.css';
import Filtro from '../Filtro';
import Head from '../Head';

const Camisetas = ({ id }) => {
  const [calcados, setCalcados] = React.useState(null);
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState(null);

  React.useEffect(() => {
    async function fetchCalcados(url) {
      try {
        setLoading(true);
        const response = await fetch(url);
        const json = await response.json();
        setCalcados(json);
      } catch (erro) {
        setError('Não foi possível fazer a listagem dos produtos :(');
      } finally {
        setLoading(false);
      }
    }
    fetchCalcados('http://localhost:8888/api/V1/categories/3');
  }, []);

  if (loading) return <div className="loading container"></div>;
  if (error) return <p>{error}</p>;
  if (calcados === null) return null;

  return (
    <main className={`${styles.calcados} container`}>
      <Head title="Webjump" description="Conheça os produtos Webjump" />
      <Filtro />
      <div className={styles.calcadosListagem}>
        <h2>Calsados</h2>
        <div className={styles.calcado}>
          <ul>
            {calcados.items.map((item) => (
              <li key={item.id}>
                <img src={require(`../../${item.image}`)} alt={item.name} />
                <p className="container">{item.name}</p>
                <span>R$ {item.price}</span>
                <button className={styles.button}>Comprar</button>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </main>
  );
};

export default Camisetas;
