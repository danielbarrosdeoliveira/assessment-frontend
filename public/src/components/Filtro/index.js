import React from 'react';
import styles from './Filtro.module.css';

const Filtro = () => {
  const [categoria, setCategoria] = React.useState(null);
  const [error, setError] = React.useState(null);

  React.useEffect(() => {
    try {
      fetch('http://localhost:8888/api/V1/categories/list')
        .then((response) => response.json())
        .then((json) => setCategoria(json));
    } catch (erro) {
      setError('Não foi possível listar os produtos :()');
    }
  }, []);

  if (error) return <p>{error}</p>;
  if (categoria === null) return null;
  return (
    <aside className={styles.filtro}>
      <h2>Filtre Por</h2>
      <div>
        <h3>Categorias</h3>
        <ul>
          {categoria.items.map((item) => (
            <li key={item.id}>{item.name}</li>
          ))}
        </ul>
      </div>
      <div>
        <h3>Cores</h3>
        <ul>
          <li>Vermelho</li>
          <li>Laranja</li>
          <li>Azul</li>
        </ul>
      </div>
      <div>
        <h3>Tipo</h3>
        <ul>
          <li>Corrida</li>
          <li>Caminhada</li>
          <li>Casual</li>
          <li>Social</li>
        </ul>
      </div>
    </aside>
  );
};

export default Filtro;
