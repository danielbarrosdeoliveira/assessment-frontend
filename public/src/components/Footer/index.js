import React from 'react';
import styles from './Footer.module.css';

const Footer = () => {
  return (
    <div className={`${styles.footer} container`}>
      <p>
        Todos os direitos reservados a Webjump | 2020
        <br />
        Desenvolvido por{' '}
        <a
          href="https://danieloliveira.info/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Daniel Oliveira
        </a>
      </p>
    </div>
  );
};

export default Footer;
