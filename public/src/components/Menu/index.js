import React from 'react';
import styles from './Menu.module.css';
import { Link } from 'react-router-dom';

const Menu = () => {
  const [menu, setMenu] = React.useState(null);
  const [error, setError] = React.useState(false);

  React.useEffect(() => {
    try {
      fetch('http://localhost:8888/api/V1/categories/list')
        .then((response) => response.json())
        .then((json) => setMenu(json));
    } catch (erro) {
      setError('Não foi possível listar os produtos :()');
    }
  }, []);

  if (error) return <p>{error}</p>;
  if (menu === null) return null;

  return (
    <nav className={`${styles.menu}`}>
      <ul className="container">
        <li>
          <Link to="/">Página Inicial</Link>
        </li>
        {menu.items.map((item) => (
          <li key={item.id}>
            <Link to={item.path}>{item.name}</Link>
          </li>
        ))}
        <li>
          <Link to="contato">contato</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Menu;
