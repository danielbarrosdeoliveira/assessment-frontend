import React from 'react';
import styles from './Calcas.module.css';
import Filtro from '../Filtro';
import Head from '../Head';

const Camisetas = ({ id }) => {
  const [calcas, setCalcas] = React.useState(null);
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState(null);

  React.useEffect(() => {
    async function fetchCamisetas(url) {
      try {
        setLoading(true);
        const response = await fetch(url);
        const json = await response.json();
        setCalcas(json);
      } catch (erro) {
        setError('Não foi possível fazer a listagem dos produtos :()');
      } finally {
        setLoading(false);
      }
    }
    fetchCamisetas('http://localhost:8888/api/V1/categories/2');
  }, []);

  if (loading) return <div className="loading container"></div>;
  if (error) return <p>{error}</p>;
  if (calcas === null) return null;

  return (
    <main className={`${styles.calcas} container`}>
      <Head title="Webjump" description="Conheça as calças do Webjump" />
      <Filtro />
      <div className={styles.calcasListagem}>
        <h2>Calcas</h2>
        <div className={styles.calca}>
          <ul>
            {calcas.items.map((item) => (
              <li key={item.id}>
                <img src={require(`../../${item.image}`)} alt={item.name} />
                <p>{item.name}</p>
                <span>R$ {item.price}</span>
                <button className={styles.button}>Comprar</button>
              </li>
            ))}
            {calcas.items.map((item) => (
              <li key={item.id}>
                <img src={require(`../../${item.image}`)} alt={item.name} />
                <p>{item.name}</p>
                <span>R$ {item.price}</span>
                <button className={styles.button}>Comprar</button>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </main>
  );
};

export default Camisetas;
