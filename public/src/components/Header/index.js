import React from 'react';
import styles from './Header.module.css';

import NavTop from '../NavTop';
import Busca from '../Busca';
import Menu from '../Menu';
import Logo from '../../media/logo-webjump.png';
import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <header className={styles.header}>
      <NavTop />
      <div className={`${styles.headerContent} container`}>
        <Link to="/">
          <img src={Logo} alt="Webjump logo" />
        </Link>
        <Busca />
      </div>
      <Menu />
    </header>
  );
};

export default Header;
