import React from 'react';
import { Link } from 'react-router-dom';
import styles from './NavTop.module.css';

const NavTop = () => {
  return (
    <nav className={styles.navTop}>
      <ul className="container">
        <li>
          <Link to="/" aria-label="Acesse sua conta">
            Acesse sua conta
          </Link>
        </li>
        <p>ou</p>
        <li>
          <Link to="/" aria-label="Cadastre-se">
            Cadastre-se
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default NavTop;
