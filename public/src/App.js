import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Home from './components/Home/Home';
import Header from './components/Header';
import Footer from './components/Footer/';
import Camisetas from './components/Camisetas';
import Calcados from './components/Calcados';
import Calcas from './components/Calcas';

function App() {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path="/" element={<Home />} exact />
        <Route path="camisetas" element={<Camisetas />} />
        <Route path="calcados" element={<Calcados />} />
        <Route path="calcas" element={<Calcas />} />
      </Routes>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
