## Tecnologias utilizadas

Foi usado HTML [HTML](https://developer.mozilla.org/pt-BR/docs/Web/HTML).
CSS3 com flex-box e grid [CSS3](https://developer.mozilla.org/pt-BR/docs/Web/CSS).
Javascript [Javascript](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript).
criado com o React [Create React App](https://github.com/facebook/create-react-app).

Utilizado a biblioteca React, pois agiliza e facilita o trabalho para o Desenvolvedor FrontEnd, como reutilização de components, SPA com react-router-dom, fazer a renderização sem a necessidade de fazer reload total da aplicação:

O projeto está estruturada:

public -> pasta onde fica os arquivos publicos.

components -> como React tem essa facilidade de se criar componentes e reutiliza-los, todos os componentes utilizados estão dentro dessa pasta.

App.js -> é o arquivo onde econtra-se as rotas para acessar o componentes.

index.js -> responsável em gerar o render do React e jogar dentro do index que tem a div root.

Para estilização utilizei o module do React, então cada componente tem seu arquivo css separado, com o nome do componente.module.css, exemplo: (Header.moduel.css).

Para comunicação com o backend, foi utilizado o fetch, map para percorrer o JSON que é retornado da API.

Os media querys foi inseridos em cada css dos components, acho que fica melhor organizado.

### OBSERVAÇÃO:

Não foi possível testar o projeto no IE, pois utilizo Ubuntu 20.04 e na distribuição não tem o IE, entretando como foi usado o React, juntamente com ele vem o Babel que da suporte de Javascript para navegadores mais antigos.

## Scritps Disponíveis

Para testar a aplicação você pode fazer o clone desse repositório:

`git start`

Acesse a pasta public

`cd public`

### `npm start`

Rodando o app localmente
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

A página será automaticamente carregada ao salvar as edições.
Você vai poder ver os erros no console.

Lembrando que é preciso rodar a API antes, que está na raiz:

basta rodar o comando `npm start`

### `npm run build`

Fazendo o build do app
Ele agrupa corretamente o React no modo de produção e otimiza a construção para obter o melhor desempenho.

O buil gera arquivos minificados.
O Aplicativo estará pronto para o deploy.

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Saiba Mais

Você pode aprender sobre o React em [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

Documentação do Reacts [React documentation](https://reactjs.org/).
